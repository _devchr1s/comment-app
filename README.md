# Comment app

Descargan el repositorio, ejecutan:
```sh
npm install
```
Para correr el servidor utilizan el comando:
```sh
node server/index.js
```

Para correr la aplicación utilizan el comando:
```sh
npm start
```