import Footer from './components/Footer/footer';
import Header from './components/Header/header';
import Container from '@mui/material/Container';
import { Component } from 'react';
import { getVersion } from './components/helpers/helper';
import ListComments from './components/ListComments/listComments';
import CommentsComponent from './components/CommentsComponent/commentsComponent';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleApp: "Listado",
      versionApp: 0,
      listCommentsComponent: true,
      addCommentComponent: false,
      editCommentComponent: false,
      idComment: null,
    }
  }

  async componentDidMount() {
    const versionData = await getVersion();
    await this.setState({ versionApp: versionData.version });
  }

  async addComment() {
    await this.setState({
      titleApp: "Creación",
      listCommentsComponent: false,
      addCommentComponent: true,
      editCommentComponent: false
    });
  }

  async editComment() {
    await this.setState({
      titleApp: "Creación",
      listCommentsComponent: false,
      addCommentComponent: false,
      editCommentComponent: true
    });
  }
  
  async backListComment() {
    await this.setState({
      titleApp: "Listado",
      listCommentsComponent: true,
      addCommentComponent: false,
      editCommentComponent: false
    });
  }

  render() {
    let { titleApp, versionApp, listCommentsComponent, addCommentComponent, editCommentComponent, idComment } = this.state;
    return (
      <Container maxWidth="md">
        <Header titleApp={titleApp} />
        {
          listCommentsComponent && 
          <ListComments 
            addComment={() => this.addComment()} 
            editComment={() => this.editComment()} />
        }
        {
          (addCommentComponent || editCommentComponent) &&
          <CommentsComponent backListComment={() => this.backListComment()} update={editCommentComponent} />
        }
        <Footer versionApp={versionApp} />
      </Container>
    );
  }
}

export default App;
