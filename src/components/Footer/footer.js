import React, { Component } from "react";

class Footer extends Component {
    render() {
        let { versionApp } = this.props;
        return <div> <h4>Versión: {versionApp}</h4> </div>
    }
}

export default Footer;