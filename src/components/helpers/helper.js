export const getVersion = async () => {
    const response = await fetch('http://127.0.0.1:3001/api/version');
    return await response.json();
}

export const getAllComments = async () => {
    const allComments = await localStorage.getItem('listCommnets');
    return (allComments === undefined || allComments === null) ? { data: [] } : JSON.parse(allComments);
}

export const getComment = async () => {
    const allComments = JSON.parse(await localStorage.getItem('listCommnets'));
    const idComment = await localStorage.getItem('idComment');
    return allComments.data.find(comment => comment.id == idComment );
}

export const editComment = async (comment) => {
    const idComment = await localStorage.getItem('idComment');
    const allComments = JSON.parse(await localStorage.getItem('listCommnets'));

    let payload = {
        id: Number(idComment),
        name: comment.name,
        email: comment.email,
        link: comment.link,
        comment: comment.comment
    }

    let index = await allComments.data.findIndex((comment) => comment.id == idComment);

    if (!comment.validateEmails) {
        let exist = 0;
        allComments.data.forEach((com) => {
            if (com.email === comment.email) {
                exist += 1;
            }
        });

        if (exist >= 1) {
            return { status: 500, response: 'El correo ya existe' };
        }

    }
    
    allComments.data[index] = payload;
    await localStorage.setItem('listCommnets', JSON.stringify({ data: allComments.data }));
    
    return { status: 200, response: 'Ok' };

}

export const saveComment = async (comment) => {
    let allComments = await localStorage.getItem('listCommnets');
    let payload = {
        id: Date.now(),
        name: comment.name,
        email: comment.email,
        link: comment.link,
        comment: comment.comment
    }
    if (allComments === undefined || allComments === null) {
        localStorage.setItem('listCommnets', JSON.stringify({ data: [payload] }));
    } else {
        allComments = JSON.parse(allComments);
        let exist = 0;
        allComments.data.forEach((com) => {
            if (com.email === comment.email) {
                exist += 1;
            }
        });

        if (exist >= 1) {
            return { status: 500, response: 'El correo ya existe' }; 
        }

        allComments.data.push(payload);
        await localStorage.setItem('listCommnets', JSON.stringify({ data: allComments.data }));
    }

    return { status: 200, response: 'Ok' };
}

export const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};

export const validLink = (link) => {
    return String(link)
            .toLocaleLowerCase()
            .match(
                /(https?:\/\/[^\s]+)/g
            )
}

export const validateName = (name) => {
    return String(name)
        .toLocaleLowerCase()
        .match(
            /^[A-Za-z ]+$/
        )
}