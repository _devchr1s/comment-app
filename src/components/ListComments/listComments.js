import React, { Component } from "react";
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { FormControl, InputAdornment, OutlinedInput, Paper, Tooltip } from "@mui/material";
import { styled } from '@mui/material/styles';
import EditIcon from '@mui/icons-material/Edit';
import { getAllComments } from "../helpers/helper";
import SearchIcon from '@mui/icons-material/Search';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#1976d280',
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: '#1976d233',
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

let typingTimer;
var doneTypingInterval = 1000;
let rows;

class ListComments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listComments: []
        }
    }

    async componentDidMount() {
        rows = await getAllComments();
        rows = rows.data;
        this.setState({ listComments: rows });
    }

    loadCommentComponent() {
        this.props.addComment();
    }

    searchData = async (event) => {
        let searchValue = event.target.value;
        let allComments = this.state.listComments;
        let newComments = [];
        if (searchValue.length > 0) {
            allComments.forEach((comment) => {
                if (
                    comment.name.startsWith(searchValue) ||
                    comment.email.startsWith(searchValue) ||
                    comment.link.startsWith(searchValue) ||
                    comment.name.includes(searchValue)
                ) {
                    newComments.push(comment);
                }
            })

            await this.setState({ listComments: newComments });
        } else {
            rows = await getAllComments();
            rows = rows.data;
            console.log(rows)
            await this.setState({ listComments: rows });
        }
    };

    keyDown = () => {
        clearTimeout(typingTimer);
    }

    keyUp = (prop) => (event) => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(() => this.searchData(event), doneTypingInterval);
    }

    render() {
        const { listComments } = this.state;
        return (
            <div className="container-comment">
                <Button
                    variant="outlined"
                    onClick={() => this.loadCommentComponent()}
                    sx={{ marginBottom: '10px' }}
                >
                    Agregar comentarios
                </Button>

                <FormControl fullWidth sx={{ marginTop: '10px', marginBottom: '10px' }}>
                    <OutlinedInput
                        onKeyDown={this.keyDown()}
                        onKeyUp={this.keyUp()}
                        startAdornment={
                            <InputAdornment position="start">
                                <SearchIcon />
                            </InputAdornment>
                        }
                    />
                </FormControl>

                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="center">Nombre</StyledTableCell>
                                <StyledTableCell align="center">Email</StyledTableCell>
                                <StyledTableCell align="center">Website</StyledTableCell>
                                <StyledTableCell align="center">Acción</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                listComments.length > 0 &&
                                listComments.map((comment) => (
                                    <StyledTableRow
                                        key={comment.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <StyledTableCell align="center" component="th" scope="row">
                                            {comment.name}
                                        </StyledTableCell>
                                        <StyledTableCell align="center">{comment.email}</StyledTableCell>
                                        <StyledTableCell align="center">
                                            {(comment.link === '') ? 'N/D' : comment.link}
                                        </StyledTableCell>
                                        <StyledTableCell align="center">
                                            <Button variant="outlined" 
                                                onClick={async () => {
                                                    await localStorage.setItem('idComment', comment.id);
                                                    this.props.editComment();
                                                }}>
                                                    <Tooltip title="Editar comentario">
                                                        <EditIcon />
                                                    </Tooltip>
                                            </Button>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                ))
                            }
                            {
                                listComments.length === 0 &&
                                <TableRow
                                    key={1}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell align="left">{'No data available'}</TableCell>
                                </ TableRow>
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        )
    }
}

export default ListComments;