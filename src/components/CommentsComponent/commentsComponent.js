import { Component } from "react";
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import Button from '@mui/material/Button';
import { Alert } from "@mui/material";
import { editComment, getComment, saveComment, validateEmail, validateName, validLink } from "../helpers/helper";

let timeout;

class CommentsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            link: '',
            comment: '',
            oldEmail: '',
            errors: []
        }
    }

    async validateForm() {
        const {name, email, link, comment, oldEmail} = this.state;
        let status = true;
        let data = '';
        clearTimeout(timeout);
        if (name.length < 3) {
            status = false;
            data = 'El campo [nombre] debe tener minimo 3 caracteres';
        } else if (name.length >= 35) {
            status = false;
            data = 'Has excedido el limite permitido de caracteres (35) en el campo [Nombre]';
        } else if (!validateName(name)) {
            status = false;
            data = 'El nombre solo puede incluir a-z, A-Z';
        } else if(email.length === 0) {
            status = false;
            data = 'El campo email es obligatorio';
        } else if (!validateEmail(email)) {
            status = false;
            data = 'El formato de email ingresado no es correcto';
        } else if (link.length > 0) {
            if (!validLink(link)) {
                status = false;
                data = 'El formato de url ingresado no es correcto';
            }
        } else if (comment.length === 0) {
            status = false;
            data = 'El campo comentario es obligatorio';
        } else if (comment.length > 100) {
            status = false;
            data = 'El campo comentario solo puede tener 100 caracteres';
        } else if (!validateName(comment)) {
            status = false;
            data = 'El campo comentario solo puede incluir a-z, A-Z';
        }

        if (status && !this.props.update) {
            const response = await saveComment({ name, email, link, comment });
            if(response.status === 500) {
                await this.setState({ errors: ['Este correo ya existe, intenta con otro'] })
                timeout = setTimeout(async () => {
                    await this.setState({ errors: [] });
                }, 3000)
            } else {
                this.props.backListComment()
            }
        } else if (status && this.props.update) {
            const validateEmails = (oldEmail == email);
            const response = await editComment({ name, email, link, comment, validateEmails});
            if (response.status === 500) {
                await this.setState({ errors: ['Este correo ya existe, intenta con otro'] })
                timeout = setTimeout(async () => {
                    await this.setState({ errors: [] });
                }, 3000)
            } else {
                this.props.backListComment()
            }
        } else {
            await this.setState({ errors: [data] })
            timeout = setTimeout( async () => {
                await this.setState({ errors: [] });
            }, 3000)
        }
    }

    async componentDidMount() {
        if (this.props.update === true) {
            let data = await getComment();
            if(data !== null || data !== undefined) {
                await this.setState({
                    name: data.name,
                    email: data.email,
                    comment: data.comment,
                    link: data.link,
                    oldEmail: data.email
                });
            } else {
                this.props.backListComment();
            }
        }
    }


    render() {
        const { errors } = this.state;
        return <Stack
            component="form"
            sx={{
                maxWidth: '500px'
            }}
            spacing={2}
            noValidate
            autoComplete="off"
        >
            <div>
                <label>Nombre: </label>
                <TextField
                    fullWidth                   
                    variant="outlined"
                    size="small"
                    value={this.state.name}
                    onChange={(data) => this.setState({ name: data.target.value })} />
            </div>

            <div>
                <label>Correo electronico: </label>
                <TextField
                    fullWidth
                    variant="outlined"
                    size="small"
                    value={this.state.email}
                    onChange={(data) => this.setState({ email: data.target.value })} />
            </div>

            <div>
                <label>Pagina web: </label>
                <TextField
                    fullWidth
                    variant="outlined"
                    size="small"
                    value={this.state.link}
                    onChange={(data) => this.setState({ link: data.target.value })} />
            </div>

            <div>
                <label>Comentario: </label>
                <TextareaAutosize
                    aria-label="empty textarea"
                    placeholder="Empty"
                    minRows={5}
                    value={this.state.comment}
                    onChange={(data) => this.setState({ comment: data.target.value })}
                    style={{ width: 500, minWidth: 440, maxWidth: 550 }}
                />
            </div>

            {
                errors.length > 0 && 
                <div>
                    <Alert severity="error">{errors[0]}</Alert>
                </div>
            }

            <div>
                <Button variant="outlined" onClick={() => this.validateForm()}>Guardar</Button> &nbsp;
                <Button variant="outlined" onClick={() => this.props.backListComment()}>Regresar</Button>
            </div>
        </Stack>
    }
}

export default CommentsComponent;