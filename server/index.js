const http = require('http');

const hostname = '127.0.0.1';
const port = 3001;

const server = http.createServer((req, res) => {

    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Request-With, Content-Type, Accept')
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS')

    const url = req.url;
    let payload = { data: {}, statusCode: 500 };

    if (url === "/api/version") {
        payload = {
            data: {
                "version": "0.1.1 todos los derechos reservados"
            },
            statusCode: 200
        };
    } else {
        payload.data = {
            error: 'Nothing here'
        }
    }

    res.writeHead(payload.statusCode, { "Content-Type": "application/json" });
    res.write(JSON.stringify(payload.data));
    res.end();
});

server.listen(port, hostname, () => {
    console.log(`El servidor se está ejecutando en http://${hostname}:${port}/`);
});